package com.megane.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.megane.entity.Team;
import com.megane.repository.TeamRepository;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/team")
@RequiredArgsConstructor
public class TeamController {
	private final TeamRepository teamRepository;

	//全チーム取得
	@CrossOrigin
	@GetMapping("/get")
	public List<Team> findAll(){
		return teamRepository.findAll();
	}

	//チーム削除
	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public void  delete(@PathVariable("id") Integer id){
		teamRepository.deleteById(id);
	}

	//チーム追加
	@CrossOrigin
	@PostMapping("/add")
	public List<Team> save(@RequestBody String json) throws JsonMappingException, JsonProcessingException {
		Team team = new Team();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);
		String teamName = node.get("name").textValue();
		team.setName(teamName);
		teamRepository.save(team);
		return teamRepository.findAll();
	}

}
